import $ from "jquery";

// ========================================================
// ========================================================

const loadFooter = async () => {
    try {
        // Extra check to see if the function should indeed be fired
        if($('#footer').length > 0) {
            const currentYear = new Date().getFullYear();
            $('#footer .year').text(currentYear);
        }
    }
    catch(err) {
        console.log(err);
    }
}

// ========================================================
// ========================================================

document.addEventListener('footer', loadFooter);
