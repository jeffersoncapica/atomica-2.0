const fs   = require('fs');
const path = require('path');

function readFile(file) {
  return fs.readFileSync(path.join(__dirname, file));
}

module.exports = {
  db_conn: {
        host: '',
		user: '',
		password: '',
		database: ''
  }
};
