import $ from "jquery";

// ========================================================
// ========================================================

const loadHomepage = async () => {
    try {
        // Extra check to see if the function should indeed be fired
        if($('#module').hasClass('homepage')) {
            // Do module stuff...

            // We're done, load page
            $('#app').addClass('loaded');
            $('#page-loader').removeClass('loading');
            PR.prettyPrint();
        }
    }
    catch(err) {
        console.log(err);
    }
}

// ========================================================
// ========================================================

document.addEventListener('homepage', loadHomepage);
