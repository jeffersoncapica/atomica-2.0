import io from 'socket.io-client';

const app = {
    // Application Constructor
    initialize: function() {
        // Do a high level dom check for module name
        const moduleDomElement = document.getElementById('module');
        const moduleName       = moduleDomElement.getAttribute('class');
        const readyEvent       = new Event(moduleName);
        const router           = new Event('router');
        const footer           = new Event('footer');
        // And dispatch corresponding event
        document.dispatchEvent(readyEvent);
        // Initialize the router as well
        document.dispatchEvent(router);
        // Initialize the footer
        document.dispatchEvent(footer);        
    }
};

export default app;
