var express       		   = require('express');
var path          		   = require('path');
var favicon       		   = require('serve-favicon');
var logger        		   = require('morgan');
var cookieParser  		   = require('cookie-parser');
var bodyParser    		   = require('body-parser');
var mysql         		   = require('mysql');
var nunjucks 			   = require('nunjucks');

var config         		   = require("./config/config");

// Homepage
var homepage               = require('./routes/homepage/homepage');
// 404 page
var pageNotFound           = require('./routes/system/page-not-found');

var app           		   = express();

// ================================================
// Request parser
// ================================================
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// ================================================
// Static directory (serves images, files etc.)
// ================================================
app.use(express.static(path.join(__dirname, 'client/prod')));

// ================================================
// Template parser
// ================================================
nunjucks.configure( path.join(__dirname, 'client/dev/template/'), {
    autoescape: true,
    express: app,
    watch: true
});

// ================================================
// Database connection
// ================================================
app.use(function(req, res, next){
	res.locals.connection = mysql.createConnection({
		host     : config.db_conn.host,
		user     : config.db_conn.user,
		password : config.db_conn.password,
		database : config.db_conn.database
	});
	next();
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// ================================================
// Route handlers
// ================================================

// Homepage
app.use('/', homepage);
// catch 404 and forward to error handler
// Leave this at the end of all routes!
app.use('*', pageNotFound);

// ================================================
// Error handlers
// ================================================
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(res.locals.error);
  // render the error page
  res.status(err.status || 500);
	res.send(JSON.stringify({
		'status': 500,
		'error': 'Internal Server Error',
		'response': 'Something went wrong.'
	}));
});

module.exports = app;
