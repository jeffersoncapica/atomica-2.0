var mysql  = require("mysql");
var config = require("./config");

var mysqlConnection = function () {
    var db;

    function createConnection() {
        var connection = mysql.createConnection({
            host     : config.db_conn.host,
            user     : config.db_conn.user,
            password : config.db_conn.password,
            database : config.db_conn.database
        });
        return connection;
    }

    return {
        getConnection: function () {
            if (!db) {
                db = createConnection();
            }
            return db;
        }
    };
};

module.exports = mysqlConnection;
