import $ from "jquery";

// ========================================================
// ========================================================

const pageNotFound = async () => {
    try {
        // Extra check to see if the function should indeed be fired
        if($('#module').hasClass('page-not-found')) {
            // Do module specific stuff...

            // We're done, load page
            $('#app').addClass('loaded');
            $('#page-loader').removeClass('loading');
        }
    }
    catch(err) {
        console.log(err);
    }
}

// ========================================================
// ========================================================

document.addEventListener('page-not-found', pageNotFound);
