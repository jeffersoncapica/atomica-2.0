// server
const express = require('express');
const path    = require('path');
const router  = express.Router();
// query functions

// =======================================================================================
// =======================================================================================

// When browsing to this url (GET)
router.get('/', (req, res, next) => {
	let execute_route = async () => {
        try {
	        // ===================================================================================

			res.render('page/system/page-not-found.html');

	        // ===================================================================================
        }
        catch(error) {
			res.send(JSON.stringify({'status': 400, 'error': error, 'response': error}));
			res.locals.connection.end();
        }
		// ===================================================================================
	}
	// Execute the async query function
	execute_route();
});

module.exports = router;
