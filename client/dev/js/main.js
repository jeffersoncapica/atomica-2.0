// Fix for async functions
import "babel-polyfill";
// Import sockets for the client
import io from 'socket.io-client';
// Import fontawesome
import fontawesome  from '@fortawesome/fontawesome-pro/js/all';

import app          from './app';
import router       from './router/router';

/* ======================================================= */
/* =============== Import all modules here =============== */
/* ======================================================= */

import footer            from './element/footer/footer';

import homepage          from './component/homepage/homepage';
// 404 - page not found
import pageNotFound      from './component/system/page-not-found';

// When window is loaded and all dependencies are ready:
window.onload=function() {
    // Socket connection to server (namespace: site-user)
    // window.siteUserSocket = io('http://atomica.capica.nl/site-user');
    // Initialize the application
    app.initialize();
};
